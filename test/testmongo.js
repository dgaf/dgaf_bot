var mongo = require('mongodb');
var monk = require('monk');
var db = monk('mongodb://tomesoftware:tome@ds021663.mlab.com:21663/dgaf');

var collection = db.get('standup');

collection.insert([
  {
    "answers" : {
      "yesterday" : "I created new unit tests for the SAR iOS app. I scheduled a meeting",
      "today" : "I want to finish testing my view controllers",
      "obstacles" : "I need approval for my culture trip so I can book flights."
    },
    "user" : "Andy Lenox",
    "timestamp" : 1464875751,
    "team" : "dgaf"
  },
  {
    "answers" : {
      "yesterday" : "I finished all testing for my view controllers on SAR iOS app",
      "today" : "I need to start a whole new project... DGAF. I'll start by making a data repo.",
      "obstacles" : "I still need approval for my culture trip so I can book flights."
    },
    "user" : "Andy Lenox",
    "timestamp" : 1464875751,
    "team" : "dgaf"
  }
], function(err, doc){
  if (err) {
    console.log("there was a problem:  " + err);
  } else {
    console.log("success!");
  }
});
