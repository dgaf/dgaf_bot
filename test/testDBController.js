var DBController = require("../lib/DBController");

var controller = new DBController();

var answers = controller.createAnswers("I worked with Damien at Ford and Jake at The Tome Software Office to reach an agreement about how to go forward with the SAR Project",
 "I am going to try to talk to Kent at Right Brain Electronics in California about getting an older e-Bike to work.", "I need Massimo to approve my culture trip.");
var response = controller.createResponse("Andy", "Tome", answers);
controller.addResponse(response);
