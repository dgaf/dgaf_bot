"use strict";

var request = require('request-json');

var client = request.createClient('http://localhost:3131/');

var getEntities = function(text, callback) {
  client.post('/ner', {"file" : text}, function(err, res, body) {
    console.log("response: " + JSON.stringify(res.body.entities));
    callback(res.body.entities);
  });
}

module.exports = getEntities;
