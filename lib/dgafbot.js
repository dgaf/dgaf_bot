/**
 * Created by melisoner on 6/2/16.
 */
'use strict';

var util = require('util');
var path = require('path');
var fs = require('fs');

var mongo = require('mongodb');
var monk = require('monk');
var db = monk('mongodb://tomesoftware:tome@ds021663.mlab.com:21663/dgaf');

var Bot = require('slackbots');
var dbController = require('./DBController');

var users = {
    andy: {
        id: 'U0SUH7EJZ',
        name: 'andy'
    },
    melis: {
        id: 'U0TE0UW01',
        name: 'melis'
    }
}

var user = users.andy;

var questions = [
    'What did you do yesterday?',
    'What are you working on today?',
    'Are there any obstacles?'
];

var response = {
    "yesterday": "",
    "today": "",
    "obstacles": ""
};


/**npm
 * Constructor function. It accepts a settings object which should contain the following keys:
 *      token : the API token of the bot (mandatory)
 *      name : the name of the bot (will default to "DgafBot")
 *      dbPath : the path to access the database (will default to "data/DgafBot.db")
 *
 * @param {object} settings
 * @constructor
 *
 */
var DgafBot = function Constructor(settings) {
    this.settings = settings;
    this.settings.name = this.settings.name || 'dgaf';
    this.user = 'dgaf';
};

// inherits methods and properties from the Bot constructor
util.inherits(DgafBot, Bot);

/**
 * Run the bot
 * @public
 */
DgafBot.prototype.run = function () {
    DgafBot.super_.call(this, this.settings);

    var q_index = 0;

    this.on('start', function () {
        this._onStart();
        this.postMessageToUser(user.name, 'are you ready for your daily?', {"as_user": true});
    });

    this.on('message', function (m) {

        //check messages only if they are coming from the user
        if (this._isChatMessage(m) && m.user === user.id && m.type === 'message') {
            if (m.text === 'yes') {
                var ts = m.ts;
                console.log('ready');
                this.postMessageToUser(user.name, questions[q_index], {"as_user": true});
                q_index++;
            }
            if (m.ts != ts) {
                this.postMessageToUser(user.name, questions[q_index], {"as_user": true});
                q_index++;
            }
            //assign correct response to correct question
            switch (q_index) {
                case 2:
                    response.yesterday = m.text;
                    break;
                case 3:
                    response.today = m.text;
                    break;
                case 4:
                    response.obstacles = m.text;
                    break;
            }

            if (q_index === 4) {
                //say bye then submit answers to the database
                this.postMessageToUser(user, "OK, take it easy. I`ll see you tomorrow.", {"as_user": true});
                this._sendResponses(user, response);
            }
        }

    });


};

DgafBot.prototype._sendResponses = function (user, response) {

    var database = new dbController();
    var answers = database.createAnswers(response.yesterday, response.today, response.obstacles);
    var _response = database.createResponse(user, 'dgaf', answers);
    database.addResponse(_response);

}

//
///**
// * On Start callback, called when the bot connects to the Slack server and access the channel
// * @private
// */
DgafBot.prototype._onStart = function (start) {
    this._loadBotUser();
};


DgafBot.prototype._onMessage = function (message) {
    this._getReadyForStandUp(message);
};


//
///**
// * Loads the user object representing the bot
// * @private
// */
DgafBot.prototype._loadBotUser = function () {
    var self = this;

    this.user = this.users.filter(function (user) {
        //if(user.name === 'andy' || user.name === 'melis') console.log(user.name, user.id);
        return user.name === self.name;
    })[0];
};


/**
 * Util function to check if a given real time message object represents a chat message
 * @param {object} message
 * @returns {boolean}
 * @private
 */
DgafBot.prototype._isChatMessage = function (message) {
    return message.type === 'message' && Boolean(message.text);
};


module.exports = DgafBot;
