"use strict";

var mongo = require('mongodb');
var monk = require('monk');
var db = monk('mongodb://tomesoftware:tome@ds021663.mlab.com:21663/dgaf');
var getEntities = require("../lib/NamedEntityRecognition");

class DBController {
  constructor() {
    this.collection = db.get('standup');
  };

  addResponse(response) {
    var appendedText = this.appendText(response);
    let collection = this.collection;
    getEntities(appendedText, function(entities){
      response.namedEntites = entities;
      collection.insert(response, function(err, doc) {
        if (err) {
          console.log("there was an error: " + err);
        } else {
          console.log("document inserted: " + doc);
        }
      });
    });
  };

  appendText(response) {
    return response.answers.yesterday + " " + response.answers.today + " " + response.answers.obstacles;
  }

  createResponse(user, team, answers) {
    return {
      "answers" : answers,
      "user" : user,
      "timestamp" : Math.floor(Date.now() / 1000),
      "team" : team
    }
  };

  createAnswers(yesterday, today, obstacles) {
    return {
      "yesterday" : yesterday,
      "today" : today,
      "obstacles" : obstacles
    }
  };
}

module.exports = DBController;
